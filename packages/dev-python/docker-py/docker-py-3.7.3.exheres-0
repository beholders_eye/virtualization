# Copyright 2015 Clément Delafargue <clement@delafargue.name>
# Distributed under the terms of the GNU General Public License v2

require pypi github [ user=docker ]
require setup-py [ import=setuptools blacklist=none has_bin=false ]

SUMMARY="An API client for docker written in Python"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/docker-pycreds[>=0.4.0][python_abis:*(-)?]
        dev-python/paramiko[>=2.4.2][python_abis:*(-)?]
        dev-python/requests[>=2.14.2][python_abis:*(-)?]
        dev-python/six[>=1.4.0][python_abis:*(-)?]
        dev-python/websocket-client[>=0.56.0][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/backports-ssl_match_hostname[>=3.5][python_abis:2.7]
            dev-python/ipaddress[>=1.0.16][python_abis:2.7]
        )
"

RESTRICT="test"

prepare_one_multibuild() {
    default

    # don't fail if docker-py is already installed
    edo sed -i \
            -e "s/import pip//" \
            -e "s/if 'docker-py'.*/if False:/" \
            setup.py
}

